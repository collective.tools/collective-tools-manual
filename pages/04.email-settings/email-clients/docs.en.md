---
title: 'Email clients'
taxonomy:
    category:
        - docs
visible: true
---

To use your email address with an external email client on your desktop or phone, add a new account and use the following settings:

## IMAP information for the incoming mail server

* Server name: mail.glesys.se
* SSL Required: Yes
* *If you see an error message when using SSL, try using TLS instead.*
* Port: 993
* Username: *your full email address*
* Password: *your password*
     
## SMTP information for the outgoing mail server

* Server name: mail.glesys.se
* SSL Required: Yes
* *If you see an error message when using SSL, try using TLS or STARTTLS instead.*
* Port: 587
* SMTP Authentication Required: Yes
* Username: *your full email address*
* Password: *your password*