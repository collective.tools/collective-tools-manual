---
title: 'Auto reply settings'
taxonomy:
    category:
        - docs
visible: true
---

The settings for auto reply for your email account can be found by logging into our email providers mail client at [mail.glesys.se](https://mail.glesys.se). Use your email address and the same password as for your Nextcloud account. Go to 
*Account settings* in the main menu for auto reply settings.