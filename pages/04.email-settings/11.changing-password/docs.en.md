---
title: 'Changing password'
taxonomy:
    category:
        - docs
visible: true
---

Your email account needs to have the same password as your Nextcloud account. Currently this means that you have to change your password in two places in order to update your password. This is something that we are working to simplify for our next update.

> The following characters are allowed in passwords: A-Z a-z 0-9 . - _ @ # $ ^ & + = ? ! *

### Change Nextcloud password

* Click on your profile picture in the top, right corner of your Nextcloud instance.
* Choose *Settings* from the drop down menu.
* Go to *Security* in the left hand menu.
* Enter your old and new password and click *Change password*.

### Change email password
The settings for auto reply for your email account can be found by logging into our email providers mail client at [mail.glesys.se](https://mail.glesys.se). Use your full email address and your original Nextcloud password. Go to Account settings in the main menu and scroll down to *Change password*.
