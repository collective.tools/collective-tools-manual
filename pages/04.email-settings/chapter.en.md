---
title: 'Email settings'
taxonomy:
    category: docs
---

# Email settings

Settings for email addresses, for individual accounts or installations with email integration.