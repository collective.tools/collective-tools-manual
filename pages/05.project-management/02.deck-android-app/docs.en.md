---
title: 'Deck Android app'
taxonomy:
    category:
        - docs
visible: true
---

There is an early version of a Deck app for Android on the on the F-Droid platform. Please note that it is still under heavy development.

First, you need to install F-Droid: <https://f-droid.org/en/> 

Then install Nextcloud Deck from the F-Droid app store: <https://f-droid.org/en/packages/it.niedermann.nextcloud.deck/>