---
title: Statutes
media_order: Stadgar-2019-05-23.pdf
---

The statutes for Collective Tools are currently only in Swedish. We are working on an English translation and will publish it here.

[Stadgar-2019-05-23.pdf](Stadgar-2019-05-23.pdf)