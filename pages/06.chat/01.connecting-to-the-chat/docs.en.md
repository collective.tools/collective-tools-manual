---
title: 'Connecting to the chat'
taxonomy:
    category:
        - docs
visible: true
---

You can use a web browser or one of the [many client applications](https://rocket.chat/download) to connect to a Rocket.Chat server.

## Using a web browser

To connect to a Rocket.Chat server through a web browser you need to enter the desired server address in the browser's address bar.

If it is a valid Rocket.Chat server address you will then be presented with the registration and login page if unauthenticated or taken to the server's homepage if authenticated.

## Using the Desktop application

To connect to a Rocket.Chat server using a client application you first need to [download](https://rocket.chat/download) the application for your operating system.

After installing and starting the application. If this is the first server you are connecting to with the desktop app you will be presented with the server connect screen. Enter the server address you want to connect to, like this: `https://chat.example.collective.tools`.

If it is a valid Rocket.Chat server address you will then be presented with the registration and login page.

### Connecting to more than one server

If you are using a Rocket.Chat desktop application you can connect to multiple Rocket.Chat servers.

To add a new server, click the **plus** icon below the server list left sidebar and follow the instructions to connect to a Rocket.Chat server using a Rocket.Chat client application.

## Using a Mobile app

To connect to a Rocket.Chat server using a mobile client application you first need to [download](https://rocket.chat/download) the application for your operating system. You can do that by searching for "Rocket.Chat" in either Google Play or App Store.

After installing and starting the application. If this is the first server you are connecting to with the desktop app you will be presented with the server connect screen. Press "Connect to a Server" and type the URL of your server.

If it is a valid Rocket.Chat server address you will then be presented with login screen, that will let you authenticate into the server using the valid options for that specific server.

---

> Source: [Connecting to a server](https://rocket.chat/docs/user-guides/connecting-to-a-server/)
