---
title: 'Email integration'
media_order: 'Screenshot_2021-02-15 Users - Collective Tools.png'
published: true
taxonomy:
    category:
        - docs
visible: true
---

If your Nextcloud installation has email integration, a new email address will be created for each new user i Nextcloud, and the email address will be deleted if you delete the Nextcloud user. There are a couple of things to remember when creating new users:
* Users need to have a display name, and it needs to be different from their username
* The username will also become the first part of the email address, like *myusername*@mydomain.com
* Enter the new email address in the email field, based on the username
* The following characters are allowed in passwords: A-Z a-z 0-9 . - _ @ # $ ^ & + = ? ! *

### Example
![](Screenshot_2021-02-15%20Users%20-%20Collective%20Tools.png)
