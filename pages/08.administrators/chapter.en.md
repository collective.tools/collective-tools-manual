---
title: 'For administrators'
taxonomy:
    category: docs
child_type: docs
---

# Administration manual

User management and settings.
