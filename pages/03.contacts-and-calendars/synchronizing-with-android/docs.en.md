---
title: 'Synchronizing with Android'
taxonomy:
    category:
        - docs
visible: true
---

1. Install [DAVx⁵ (formerly known as DAVDroid)](https://www.davx5.com/download/) on your Android device.
2. Create a new account ("+" button).
3. Select **Connection with URL and username**.
   **Base URL:** URL of your Nextcloud instance (e.g. `https://sub.example.com/remote.php/dav`)
   **Contact Group Method:** As credentials
4. Chose the option ``Groups are per-contact categories``.
5. Click **Connect**.
6. Select the data you want to sync.

! Enter your email address as DAVx⁵ account name (mandatory if you want to be able to send calendar invitation)

---

> Source: [Synchronizing with Android](https://docs.nextcloud.com/server/stable/user_manual/pim/sync_android.html) by [Nextcloud GmbH](http://nextcloud.com/) is licenced under [CC BY 3.0](https://creativecommons.org/licenses/by/3.0/).