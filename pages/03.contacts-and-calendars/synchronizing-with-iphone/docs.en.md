---
title: 'Synchronizing with iPhone'
taxonomy:
    category:
        - docs
visible: true
---

Calendar
--------

1. Open the settings application.
1. Select Mail, Contacts, Calendars.
1. Select Add Account.
1. Select Other as account type.
1. Select Add CalDAV account.
1. For server, type ``example.com/remote.php/dav/principals/users/USERNAME/``
1. Enter your user name and password.
1. Select Next.
1. If your server does not support SSL, a warning will be displayed.
   Select Continue.
1. If the iPhone is unable to verify the account information perform the
   following steps:

   -  Select OK.
   -  Select advanced settings.
   -  If your server does not support SSL, make sure Use SSL is set to OFF.
   -  Change port to 80.
   -  Go back to account information and hit Save.

Your calendar will now be visible in the Calendar application


Contacts
--------

1. Open the settings application.
1. Select Mail, Contacts, Calendars.
1. Select Add Account.
1. Select Other as account type.
1. Select Add CardDAV account.
1. For server, type ``example.com/remote.php/dav/principals/users/USERNAME/``
1. Enter your user name and password.
1. Select Next.
1. If your server does not support SSL, a warning will be displayed.
   Select Continue.
1. If the iPhone is unable to verify the account information perform the
   following:

   -  Select OK.
   -  Select advanced settings.
   -  If your server does not support SSL, make sure Use SSL is set to OFF.
   -  Change port to 80.
   -  Go back to account information and hit Save.

You should now find your contacts in the address book of your iPhone.
If it's still not working, have a look at the [Troubleshooting Contacts & Calendar](https://docs.nextcloud.org/server/14/admin_manual/issues/index.html#troubleshooting-contacts-calendar) guide.

---

> Source: [Synchronizing with iOS](https://docs.nextcloud.com/server/stable/user_manual/pim/sync_ios.html) by [Nextcloud GmbH](http://nextcloud.com/) is licenced under [CC BY 3.0](https://creativecommons.org/licenses/by/3.0/).