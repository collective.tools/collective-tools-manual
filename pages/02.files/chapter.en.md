---
title: 'Files and Documents'
taxonomy:
    category: docs
    tag: ''
process:
    markdown: true
    twig: true
visible: true
---

# Files and Documents

Working with files in the web interface and syncing to your desktop and phone.