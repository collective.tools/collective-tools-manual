---
title: 'Synchronizing files'
taxonomy:
    category:
        - docs
visible: true
---

For synchronizing files with your desktop computer, we recommend using the
[Nextcloud Sync Client](https://nextcloud.com/install/#install-clients) for Windows, macOS and Linux.

The Nextcloud Desktop Sync Client enables you to connect to your Nextcloud Server.
You can create folders in your home directory, and keep the contents of those
folders synced with your Nextcloud server. Simply copy a file into the directory
and the Nextcloud desktop client does the rest. Make a change to the files on one
computer, it will flow across the others using these desktop sync clients.
You will always have your latest files with you wherever you are.

Its usage is documented separately in the [Nextcloud Desktop Client Manual](https://docs.nextcloud.com/desktop/2.3).

Mobile clients
--------------

Visit your Personal page in your Nextcloud Web interface to find download links
for Android and iOS mobile sync clients. Or, visit the [Nextcloud download page](https://nextcloud.com/install).

---

> Source: [Desktop and mobile synchronization](https://docs.nextcloud.com/server/stable/user_manual/files/desktop_mobile_sync.html) by [Nextcloud GmbH](http://nextcloud.com/) is licenced under [CC BY 3.0](https://creativecommons.org/licenses/by/3.0/).
