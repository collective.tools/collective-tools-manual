---
title: Introduction
taxonomy:
    category:
        - docs
visible: true
---

Welcome to the Collective Tools manual for users and administrators! This manual will cover the basics in using Nextcloud and Rocket.Chat and hopefully give you everything you need to get started and adapt the programs to your needs.

If you want to learn more, here are the complete  manuals for [Nextcloud](https://docs.nextcloud.com/) and [Rocket.Chat](https://rocket.chat/docs/). And if you want even more support, we recommend searching the user forums for [Nextcloud](https://help.nextcloud.com/) and [Rocket.Chat](https://forums.rocket.chat/).

Want to contribute to this user manual or provide a translation? Do a pull request on [GitLab](https://gitlab.com/collective.tools/collective-tools-manual/), or get in touch on [hello@collective.tools](mailto:hello@collective.tools).