---
title: 'Logging in'
media_order: 'Skärmavbild 2019-05-29 kl. 15.03.11.png,logga in.gif'
taxonomy:
    category:
        - docs
visible: true
---

There are two different places to log in to for the Collective Tools programs:

* Nextcloud for files, calendars, contacts and project management
* Rocket.Chat for the chat.
 
Depending on if you use the browser or apps for desktop or mobile, this will look slightly different. If you only use the browser, you can access Rocket.Chat from your Nextcloud menu, and only need to remember one web address.

You will use your Nextcloud account to log in to both programs, so you only need one user account.

## Nextcloud

### In your browser
Log in at <https://your-organisation-name.collective.tools> (replace "your-organisation-name").

### Desktop
Download the Nextcloud app at <https://nextcloud.com/install/>. Fill in the address to Nextcloud as your server (<https://your-organisation-name.collective.tools>) and your username and password to log in. More documentation can be found in the [Nextcloud Desktop Client Manual](https://docs.nextcloud.com/desktop/2.3).

### Mobile
Download the Nextcloud app in the app store for Android or iPhone. Fill in the address to Nextcloud as your server (<https://your-organisation-name.collective.tools>) and your username and password to log in.

## Rocket.Chat

### In your browser
You can access the chat directly from the top menu in Nextcloud.

![](logga%20in.gif)

Another option is to go directly to the chat at <https://chat.your-organisation-name.collective.tools>.

Use the "Log in with Nextcloud" button to log in.

### Desktop
Download the desktop app from <https://rocket.chat/install>.

Fill in <https://chat.your-organisation-name.collective.tools> as your server url.

Use the "Log in with Nextcloud" button to log in.

### Mobile
Download the Rocket.Chat app from the app store for Android or iPhone. 

Fill in <https://chat.your-organisation-name.collective.tools> as your server url.

Use the "Log in with Nextcloud" button to log in.


### Log in with Nextcloud

1. At the log in screen, click the button "Log in with Nextcloud".
2. You will be redirected to a page with a button to log in. Click the log in button.
4. Then log in with your Nextcloud username and password.
5. Grant Rocket.Chat access to your account by clicking "Grant access".
6. You will be redirected back to Rocket.Chat.
