---
title: 'Export data and delete account (GDPR)'
media_order: 'Skärmavbild 2019-05-27 kl. 16.07.25.png,Skärmavbild 2019-05-27 kl. 16.08.11.png,Skärmavbild 2019-05-27 kl. 16.14.16.png,Skärmavbild 2019-05-27 kl. 16.18.49.png,Skärmavbild 2019-05-27 kl. 16.19.10.png,Skärmavbild 2019-05-27 kl. 16.26.14.png,Skärmavbild 2019-05-27 kl. 16.28.25.png,Skärmavbild 2019-05-27 kl. 16.41.43.png'
taxonomy:
    category:
        - docs
visible: true
---

## Exporting your data

### Nextcloud

**Files:** Go to the file view in the top menu. Select all files and folders by clicking the top checkbox. In the "Actions" menu, click "Download".

![](Ska%CC%88rmavbild%202019-05-27%20kl.%2016.18.49.png)

![](Ska%CC%88rmavbild%202019-05-27%20kl.%2016.19.10.png)

**Contacts:** Go to Contacts in the top menu. Click "Settings" in the bottom left corner. Click the menu next to your address book and then "Download".

![](Ska%CC%88rmavbild%202019-05-27%20kl.%2016.26.14.png)

**Calendars:** Go to Calendar in the top menu. Click the menu next to your calendars and then "Download". Do this for all your calendars.

![](Ska%CC%88rmavbild%202019-05-27%20kl.%2016.28.25.png)

**Deck:** Your user data has to be exported by someone with server access. Please contact <admin@collective.tools> if you need your Deck data as a JSON-file.

### Rocket.Chat

Click your user image in the top left corner and go to "My Account".

![](Ska%CC%88rmavbild%202019-05-27%20kl.%2016.14.16.png)

At the bottom of the screen, click the button "Export my data". You will get an email with a link to your data when it has been processed.

![](Ska%CC%88rmavbild%202019-05-27%20kl.%2016.41.43.png)

## Deleting you account

### Nextcloud

In Nextcloud, go to Settings in the top right corner menu.

![](Ska%CC%88rmavbild%202019-05-27%20kl.%2016.07.25.png)

In the left column, go to "Delete account".

![](Ska%CC%88rmavbild%202019-05-27%20kl.%2016.08.11.png)

Check the box and press the button to delete your account and all your data.

### Rocket.Chat

Click your user image in the top left corner and go to "My Account".

![](Ska%CC%88rmavbild%202019-05-27%20kl.%2016.14.16.png)

Go to "Profile" in the left column.

At the bottom of the screen, click the button "Delete my account".
