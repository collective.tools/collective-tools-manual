---
title: 'Nextcloud settings'
media_order: 'oc_personal_settings_dropdown.png,personal_settings.png'
taxonomy:
    category:
        - docs
visible: true
---

As a user, you can manage your personal settings.

To access your personal settings:

1. Click on your profile picture in the top, right corner of your Nextcloud instance.

   The Personal Settings Menu opens.

	![screenshot of user menu at top-right of Nextcloud Web GUI](oc_personal_settings_dropdown.png)

   *Personal Settings Menu*

2. Choose *Settings* from the drop down menu.

      ![screenshot of user's Personal settings page](personal_settings.png)

The options listed in the Personal Settings Page depend on the applications that
are enabled by the administrator. Some of the features you will see
include the following:

* Usage and available quota
* Manage your profile picture
* Full name (You can make this anything you want, as it is separate from your
  Nextcloud login name, which is unique and cannot be changed)
* Email address
* List of your Group memberships
* Change your password
* Choose the language for your Nextcloud interface
* Links to desktop and mobile apps
* Manage your Activity stream and notifications
* Default folder to save new documents to
* Your Federated sharing ID
* Social sharing links
* Nextcloud version

---

> Source: [Accessing your files using the Nextcloud Web interface](https://docs.nextcloud.com/server/stable/user_manual/files/access_webgui.html) by [Nextcloud GmbH](http://nextcloud.com/) is licenced under [CC BY 3.0](https://creativecommons.org/licenses/by/3.0/).