---
title: 'User preferences'
taxonomy:
    category: docs
---

# User preferences

User settings in Nextcloud and Rocket.Chat.